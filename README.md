# GitLab Issues and Merge Requests Report

## Installation

The script itself doesn't require installation, but does depend on two python
packages which must be installed:

```
python3 -m pip install ruamel.yaml
python3 -m pip install python-gitlab
```

## Usage

Clone this repository, then navigate to the folder containing the script. And
run:

```
python3 gitlab_issues_and_merge_requests_report.py {PROJECT_ID} -m {NUMBER_OF_MONTHS}
```
Replace `{PROJECT_ID}` with the ID of the project you need results for. The
project ID can be found on the project's main landing pag on GitLAb, just under
the project title. Replace `{NUMBER_OF_MONTHS}` with the number of months you
want to look back over. Results will always start from the first of a month.

eg
```
python3 gitlab_issues_and_merge_requests_report.py 10630612 -m 3
```

Leave out the `-m` argument to see a full list of all issues and merge requests.

The script sends output direct to stdout. Use shell redirection (`>`)to send the
results into a text file:

eg
```
python3 gitlab_issues_and_merge_requests_report.py 10630612 -m 3 > results.txt
```

## Help text:

```
Creates a list of merge requests and issues that have been opened and/or closed in each month, for a given GitLab
project.

positional arguments:
  project_id            ID of the GitLab project. Can be found on the project's main landing page on GitLab, just
                        under the project title. Should be an integer, usually 7 or 8 digits.

optional arguments:
  -h, --help            show this help message and exit
  --gitlab_url GITLAB_URL
                        The URL of the GitLab instance to interrogate. Defaults to https://gitlab.com
  -m GO_BACK_MONTHS, --months GO_BACK_MONTHS
                        The number of months to go back, when showing information, cutoff is rounded back to the
                        start of the month. (eg. if the current date is May 12th, and '--months 2' is given, then
                        the script returns events from March, April and May, starting on March 1st.)
```

## Sample output

```
2021-03 (Mar 2021):
  issues created:
  - '(2021-03-08) 119: Define a "display_version" for each client, server and remote_asset
    server in matrix.yml'
  - '(2021-03-05) 118: Use our own docker image for GitLab CI'
  issues closed:
  - "(2021-03-05) 114: Designing a generic 'version definion' system for clients and\
    \ servers (Second version)"
  merge_requests created:
  - '(2021-03-05) 223: Tidy up (directory structure and .gitlab-ci.yml)'
  - '(2021-03-02) 222: Update goma to v0.0.17 and update the goma_server_patches'
  - '(2021-03-01) 221: Automated update 2021-03-01'
  merge_requests closed:
  - '(2021-03-03) 215: WIP: Implement a central version control, with jsonnet templates'
2021-02 (Feb 2021):
  issues created:
  - "(2021-02-18) 114: Designing a generic 'version definion' system for clients and\
    \ servers (Second version)"
  merge_requests closed:
  - '(2021-02-24) 217: update remote_asset to 2021-01-23 & update config'
  - '(2021-02-08) 209: Automated update 2021-02-01'
  - '(2021-02-26) 208: Add labels for compatibility with SELinux-enabled distros'
```
