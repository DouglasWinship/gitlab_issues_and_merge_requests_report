#! /usr/bin/env python3
"""Creates a list of merge requests and issues that have been opened and/or closed in
each month, for a given GitLab project."""

import argparse
import datetime
import sys
import gitlab
from ruamel.yaml import YAML

PROJECT_ID = 10630612
OUTPUT_FILE = "issues_and_merge_requests_report.yml"
NUMBER_MONTHS = 5

DATE_FORMAT_STRING = "%Y-%m-%dT%H:%M:%S.%fZ"


def get_start_date(number_of_months):
    """Calculates a start date by going back x months, and then rounding back to the
    start of the month."""
    if number_of_months is None or number_of_months == 0:
        return datetime.date(year=1900, month=1, day=1)
    current_date = datetime.datetime.now()
    return datetime.date(
        year=current_date.year + ((current_date.month - number_of_months) // 12),
        month=(current_date.month - number_of_months) % 12 + 1,
        day=1,
    )


def get_date(date_string):
    """Returns a datetime date object from a date_string."""
    if date_string is None:
        return None
    return datetime.datetime.strptime(date_string, DATE_FORMAT_STRING).date()


def get_month(date_object):
    """Returns a string that represents the month and year of a date."""
    if date_object is None:
        return None
    return date_object.strftime("%Y-%m (%b %Y)")


def main(args):
    """Main function. See module docstring."""
    glb = gitlab.Gitlab(args.gitlab_url)
    project = glb.projects.get(args.project_id)

    start_date = get_start_date(args.go_back_months)
    query_parameters = {"updated_after": start_date.isoformat()}

    issues = project.issues.list(all=True, query_parameters=query_parameters,)
    merge_requests = project.mergerequests.list(
        all=True, query_parameters=query_parameters
    )

    months = {}

    def insert_item(item, date_string, key_name):
        date_object = get_date(date_string)
        if date_object is not None and date_object >= start_date:
            relevant_month = get_month(date_object)
            month_dict = months.setdefault(relevant_month, {})
            items_list = month_dict.setdefault(key_name, [])
            items_list.append(f"({date_object}) {item.iid}: {item.title}")

    for issue in issues:
        insert_item(issue, issue.created_at, "issues created")
        insert_item(issue, issue.closed_at, "issues closed")
    for merge_request in merge_requests:
        insert_item(merge_request, merge_request.created_at, "merge_requests created")
        insert_item(merge_request, merge_request.closed_at, "merge_requests closed (not merged)")
        insert_item(merge_request, merge_request.merged_at, "merge_requests merged")

    for month_dict in months.values():
        for item_list in month_dict.values():
            item_list.sort(reverse=True)

    yaml = YAML()
    yaml.dump(months, sys.stdout)


def get_args():
    """Parse command line arguments."""
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument(
        "project_id",
        help=(
            "ID of the GitLab project. Can be found on the project's main landing page"
            " on GitLab, just under the project title. Should be an integer, usually 7"
            " or 8 digits."
        ),
    )
    arg_parser.add_argument(
        "--gitlab_url",
        default="https://gitlab.com",
        help=(
            "The URL of the GitLab instance to interrogate. Defaults to"
            " https://gitlab.com"
        ),
    )
    arg_parser.add_argument(
        "-m",
        "--months",
        type=int,
        dest="go_back_months",
        help=(
            "The number of months to go back, when showing information, cutoff is"
            " rounded back to the start of the month. (eg. if the current date is May"
            " 12th, and '--months 3' is given, then the script returns events from"
            " March, April and May, starting on March 1st.)"
        ),
    )
    return arg_parser.parse_args()


if __name__ == "__main__":
    ARGS = get_args()
    main(ARGS)
